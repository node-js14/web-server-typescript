import express from 'express';
import cors from 'cors';
import userRoutes from '../routes/user.routes';
import db from '../database/connection';

const api = process.env.API_PATH;

class Server {
  private app: express.Application;
  private port: string;
  private apiPaths = {
    user: `${api}/user`,
  };

  constructor() {
    this.app = express();
    this.port = process.env.PORT || '8000';

    // method initials
    this.dbConnection();
    this.middleware();
    this.routes();
  }

  async dbConnection() {
    try {
      await db.authenticate();
      console.log('database connection...');
    } catch (err) {
      console.log(err);
      throw new Error('err' + err);
    }
  }

  middleware() {
    //  TODO CORS
    this.app.use(cors());
    //  TODO READING OF THE BODY
    this.app.use(express.json());
    //  TODO FOLDER PUBLIC
    this.app.use(express.static('public'));
  }

  routes() {
    this.app.use(this.apiPaths.user, userRoutes);
  }

  listen() {
    this.app.listen(this.port, () => {
      console.log(`Run server http://localhost:${this.port}`);
    });
  }
}

export { Server };
