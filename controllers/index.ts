import {
  deleteUser,
  getUser,
  postUser,
  putUser,
  getUsers,
  getUsersAll,
} from './user.controllers';

export { deleteUser, getUser, postUser, putUser, getUsers, getUsersAll };
