import { request, response } from 'express';
import Users from '../models/user.models';

const getUsers = async (req = request, res = response) => {
  res.json({ msg: 'getUser xd' });
};

const getUser = async (req = request, res = response) => {
  const { id } = req.params;
  const user = await Users.findByPk(id);

  if (user) {
    res.json({ msg: 'list user', user });
  } else {
    res.status(404).json({ msg: 'The user does not exist' });
  }
};

const getUsersAll = async (req = request, res = response) => {
  const user = await Users.findAll();

  if (user.length !== 0) {
    res.json({ msg: 'list users', user });
  } else {
    res.status(404).json({ msg: 'No users' });
  }
};

const postUser = async (req = request, res = response) => {
  const { body } = req;
  try {
    const existEmail = await Users.findOne({
      where: {
        email: body.email,
      },
    });

    if (existEmail) {
      return res.status(400).json({ msg: 'The mail already exists' });
    }

    const user = Users.build(body);
    await user.save();
    res.status(202).json({ msg: 'User create', user });
  } catch ({ errors }) {
    res.status(500).json({ msg: 'Error creating', errors });
  }
};

const putUser = async (req = request, res = response) => {
  const { id } = req.params;
  // const { email, ...data } = req.body;
  const { body } = req;

  try {
    const user = await Users.findByPk(id);
    if (!user) {
      return res.status(400).json({ msg: 'The user does not exist' });
    }

    await user.update(body);
    res.status(200).json({ user });
  } catch (err) {
    res.status(500).json({ msg: 'Error creating' });
  }
};

const deleteUser = async (req = request, res = response) => {
  const { id } = req.params;
  // const { email, ...data } = req.body;
  const { body } = req;

  try {
    const user = await Users.findByPk(id);
    if (!user) {
      return res.status(400).json({ msg: 'The user does not exist' });
    }

    await user.update({ state: false });
    res.status(202).json({ msg: 'User delete' });
  } catch (err) {
    res.status(500).json({ msg: 'Error creating' });
  }
};

export { getUser, getUsers, getUsersAll, postUser, putUser, deleteUser };
