"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.getUsersAll = exports.getUsers = exports.putUser = exports.postUser = exports.getUser = exports.deleteUser = void 0;
const user_controllers_1 = require("./user.controllers");
Object.defineProperty(exports, "deleteUser", { enumerable: true, get: function () { return user_controllers_1.deleteUser; } });
Object.defineProperty(exports, "getUser", { enumerable: true, get: function () { return user_controllers_1.getUser; } });
Object.defineProperty(exports, "postUser", { enumerable: true, get: function () { return user_controllers_1.postUser; } });
Object.defineProperty(exports, "putUser", { enumerable: true, get: function () { return user_controllers_1.putUser; } });
Object.defineProperty(exports, "getUsers", { enumerable: true, get: function () { return user_controllers_1.getUsers; } });
Object.defineProperty(exports, "getUsersAll", { enumerable: true, get: function () { return user_controllers_1.getUsersAll; } });
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW5kZXguanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyIuLi8uLi9jb250cm9sbGVycy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOzs7QUFBQSx5REFPNEI7QUFFbkIsMkZBUlAsNkJBQVUsT0FRTztBQUFFLHdGQVBuQiwwQkFBTyxPQU9tQjtBQUFFLHlGQU41QiwyQkFBUSxPQU00QjtBQUFFLHdGQUx0QywwQkFBTyxPQUtzQztBQUFFLHlGQUovQywyQkFBUSxPQUkrQztBQUFFLDRGQUh6RCw4QkFBVyxPQUd5RCJ9