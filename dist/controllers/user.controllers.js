"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUser = exports.putUser = exports.postUser = exports.getUsersAll = exports.getUsers = exports.getUser = void 0;
const express_1 = require("express");
const user_models_1 = __importDefault(require("../models/user.models"));
const getUsers = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    res.json({ msg: 'getUser xd' });
});
exports.getUsers = getUsers;
const getUser = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const user = yield user_models_1.default.findByPk(id);
    if (user) {
        res.json({ msg: 'list user', user });
    }
    else {
        res.status(404).json({ msg: 'The user does not exist' });
    }
});
exports.getUser = getUser;
const getUsersAll = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    const user = yield user_models_1.default.findAll();
    if (user.length !== 0) {
        res.json({ msg: 'list users', user });
    }
    else {
        res.status(404).json({ msg: 'No users' });
    }
});
exports.getUsersAll = getUsersAll;
const postUser = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const existEmail = yield user_models_1.default.findOne({
            where: {
                email: body.email,
            },
        });
        if (existEmail) {
            return res.status(400).json({ msg: 'The mail already exists' });
        }
        const user = user_models_1.default.build(body);
        yield user.save();
        res.status(202).json({ msg: 'User create', user });
    }
    catch ({ errors }) {
        res.status(500).json({ msg: 'Error creating', errors });
    }
});
exports.postUser = postUser;
const putUser = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    // const { email, ...data } = req.body;
    const { body } = req;
    try {
        const user = yield user_models_1.default.findByPk(id);
        if (!user) {
            return res.status(400).json({ msg: 'The user does not exist' });
        }
        yield user.update(body);
        res.status(200).json({ user });
    }
    catch (err) {
        res.status(500).json({ msg: 'Error creating' });
    }
});
exports.putUser = putUser;
const deleteUser = (req = express_1.request, res = express_1.response) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    // const { email, ...data } = req.body;
    const { body } = req;
    try {
        const user = yield user_models_1.default.findByPk(id);
        if (!user) {
            return res.status(400).json({ msg: 'The user does not exist' });
        }
        yield user.update({ state: false });
        res.status(202).json({ msg: 'User delete' });
    }
    catch (err) {
        res.status(500).json({ msg: 'Error creating' });
    }
});
exports.deleteUser = deleteUser;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoidXNlci5jb250cm9sbGVycy5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbIi4uLy4uL2NvbnRyb2xsZXJzL3VzZXIuY29udHJvbGxlcnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7Ozs7Ozs7O0FBQUEscUNBQTRDO0FBQzVDLHdFQUEwQztBQUUxQyxNQUFNLFFBQVEsR0FBRyxDQUFPLEdBQUcsR0FBRyxpQkFBTyxFQUFFLEdBQUcsR0FBRyxrQkFBUSxFQUFFLEVBQUU7SUFDdkQsR0FBRyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSxZQUFZLEVBQUUsQ0FBQyxDQUFDO0FBQ2xDLENBQUMsQ0FBQSxDQUFDO0FBZ0ZnQiw0QkFBUTtBQTlFMUIsTUFBTSxPQUFPLEdBQUcsQ0FBTyxHQUFHLEdBQUcsaUJBQU8sRUFBRSxHQUFHLEdBQUcsa0JBQVEsRUFBRSxFQUFFO0lBQ3RELE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQzFCLE1BQU0sSUFBSSxHQUFHLE1BQU0scUJBQUssQ0FBQyxRQUFRLENBQUMsRUFBRSxDQUFDLENBQUM7SUFFdEMsSUFBSSxJQUFJLEVBQUU7UUFDUixHQUFHLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLFdBQVcsRUFBRSxJQUFJLEVBQUUsQ0FBQyxDQUFDO0tBQ3RDO1NBQU07UUFDTCxHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLEdBQUcsRUFBRSx5QkFBeUIsRUFBRSxDQUFDLENBQUM7S0FDMUQ7QUFDSCxDQUFDLENBQUEsQ0FBQztBQXFFTywwQkFBTztBQW5FaEIsTUFBTSxXQUFXLEdBQUcsQ0FBTyxHQUFHLEdBQUcsaUJBQU8sRUFBRSxHQUFHLEdBQUcsa0JBQVEsRUFBRSxFQUFFO0lBQzFELE1BQU0sSUFBSSxHQUFHLE1BQU0scUJBQUssQ0FBQyxPQUFPLEVBQUUsQ0FBQztJQUVuQyxJQUFJLElBQUksQ0FBQyxNQUFNLEtBQUssQ0FBQyxFQUFFO1FBQ3JCLEdBQUcsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7S0FDdkM7U0FBTTtRQUNMLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLFVBQVUsRUFBRSxDQUFDLENBQUM7S0FDM0M7QUFDSCxDQUFDLENBQUEsQ0FBQztBQTJEMEIsa0NBQVc7QUF6RHZDLE1BQU0sUUFBUSxHQUFHLENBQU8sR0FBRyxHQUFHLGlCQUFPLEVBQUUsR0FBRyxHQUFHLGtCQUFRLEVBQUUsRUFBRTtJQUN2RCxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDO0lBQ3JCLElBQUk7UUFDRixNQUFNLFVBQVUsR0FBRyxNQUFNLHFCQUFLLENBQUMsT0FBTyxDQUFDO1lBQ3JDLEtBQUssRUFBRTtnQkFDTCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7YUFDbEI7U0FDRixDQUFDLENBQUM7UUFFSCxJQUFJLFVBQVUsRUFBRTtZQUNkLE9BQU8sR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUseUJBQXlCLEVBQUUsQ0FBQyxDQUFDO1NBQ2pFO1FBRUQsTUFBTSxJQUFJLEdBQUcscUJBQUssQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDLENBQUM7UUFDL0IsTUFBTSxJQUFJLENBQUMsSUFBSSxFQUFFLENBQUM7UUFDbEIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsYUFBYSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7S0FDcEQ7SUFBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7UUFDbkIsR0FBRyxDQUFDLE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxJQUFJLENBQUMsRUFBRSxHQUFHLEVBQUUsZ0JBQWdCLEVBQUUsTUFBTSxFQUFFLENBQUMsQ0FBQztLQUN6RDtBQUNILENBQUMsQ0FBQSxDQUFDO0FBc0N1Qyw0QkFBUTtBQXBDakQsTUFBTSxPQUFPLEdBQUcsQ0FBTyxHQUFHLEdBQUcsaUJBQU8sRUFBRSxHQUFHLEdBQUcsa0JBQVEsRUFBRSxFQUFFO0lBQ3RELE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQzFCLHVDQUF1QztJQUN2QyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDO0lBRXJCLElBQUk7UUFDRixNQUFNLElBQUksR0FBRyxNQUFNLHFCQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLHlCQUF5QixFQUFFLENBQUMsQ0FBQztTQUNqRTtRQUVELE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUN4QixHQUFHLENBQUMsTUFBTSxDQUFDLEdBQUcsQ0FBQyxDQUFDLElBQUksQ0FBQyxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7S0FDaEM7SUFBQyxPQUFPLEdBQUcsRUFBRTtRQUNaLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQztLQUNqRDtBQUNILENBQUMsQ0FBQSxDQUFDO0FBb0JpRCwwQkFBTztBQWxCMUQsTUFBTSxVQUFVLEdBQUcsQ0FBTyxHQUFHLEdBQUcsaUJBQU8sRUFBRSxHQUFHLEdBQUcsa0JBQVEsRUFBRSxFQUFFO0lBQ3pELE1BQU0sRUFBRSxFQUFFLEVBQUUsR0FBRyxHQUFHLENBQUMsTUFBTSxDQUFDO0lBQzFCLHVDQUF1QztJQUN2QyxNQUFNLEVBQUUsSUFBSSxFQUFFLEdBQUcsR0FBRyxDQUFDO0lBRXJCLElBQUk7UUFDRixNQUFNLElBQUksR0FBRyxNQUFNLHFCQUFLLENBQUMsUUFBUSxDQUFDLEVBQUUsQ0FBQyxDQUFDO1FBQ3RDLElBQUksQ0FBQyxJQUFJLEVBQUU7WUFDVCxPQUFPLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLHlCQUF5QixFQUFFLENBQUMsQ0FBQztTQUNqRTtRQUVELE1BQU0sSUFBSSxDQUFDLE1BQU0sQ0FBQyxFQUFFLEtBQUssRUFBRSxLQUFLLEVBQUUsQ0FBQyxDQUFDO1FBQ3BDLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLGFBQWEsRUFBRSxDQUFDLENBQUM7S0FDOUM7SUFBQyxPQUFPLEdBQUcsRUFBRTtRQUNaLEdBQUcsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUMsSUFBSSxDQUFDLEVBQUUsR0FBRyxFQUFFLGdCQUFnQixFQUFFLENBQUMsQ0FBQztLQUNqRDtBQUNILENBQUMsQ0FBQSxDQUFDO0FBRTBELGdDQUFVIn0=