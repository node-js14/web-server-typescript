import { Sequelize } from 'sequelize';

const db = new Sequelize(
  `${process.env.MYSQLDATABASE}`,
  `${process.env.MYSQLUSER}`,
  process.env.MYSQLPASSWORD,
  {
    host: process.env.MYSQLHOST,
    dialect: 'mysql',
  }
);

export default db;
